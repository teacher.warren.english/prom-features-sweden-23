﻿namespace PromFeatsSweden23
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region
            ChaosBasket<bool, string> myBasket = new();
            myBasket.AddToBasket(true);
            myBasket.AddToBasket2("Hey");
            myBasket.PrintContents();

            Console.WriteLine($"T = {myBasket.GetTypeFromT()} ^ U = {myBasket.GetTypeFromU()}");
            #endregion
            #region
            List<int> myNumbers = new List<int>() { 1, 2, 3, -5, -2, 0, 42, 13, -55, -6 };

            List<int> filteredList = FilterNumbers(FilterNumbers(myNumbers, IsNegative), IsOdd);

            foreach (int num in filteredList)
                Console.WriteLine(num);
            #endregion
        }
        
        #region
        public delegate bool NumberFilter(int num);

        public static bool IsEven(int num)
        {
            return num % 2 == 0;
        }

        public static bool IsOdd(int num)
        {
            return num % 2 != 0;
        }

        public static bool IsPositive(int num)
        {
            return num >= 0;
        }

        public static bool IsNegative(int num)
        {
            return !IsPositive(num);
        }

        public static List<int> FilterNumbers(List<int> numArr, NumberFilter filter)
        {
            List<int> output = new List<int>();

            foreach (int num in numArr)
            {
                if (filter(num))
                    output.Add(num);
            }

            return output;
        }
        #endregion
    }
}