﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PromFeatsSweden23
{
    internal class ChaosBasket<T, U>
    {
        public T[] Basket { get; set; } = new T[10];

        public U[] Basket2 { get; set; } = new U[10];

        public void AddToBasket(T item)
        {
            Random random = new();
            Basket[random.Next(Basket.Length)] = item;
        }

        public string GetTypeFromT()
        {
            return typeof(T).ToString();
        }

        public string GetTypeFromU()
        {
            return typeof(U).ToString();
        }

        public void AddToBasket2(U item)
        {
            Random random = new();
            Basket2[random.Next(Basket2.Length)] = item;
        }

        public void PrintContents()
        {
            foreach (var item in Basket)
                Console.WriteLine(item);

            Console.WriteLine();

            foreach (var item in Basket2)
                Console.WriteLine(item);
        }
    }
}
